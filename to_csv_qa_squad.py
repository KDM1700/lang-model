from numpy.core.records import record
import pandas as pd
from tqdm import tqdm
import sys


DATA_DIR = "../data/"
JSON_EXT = '.jsonl'
CSV_EXT = '.csv'
MAX_TEXT_LEN = 800
MAX_QUESTION_LEN = 100

FILE_NAME = sys.argv[1]

#read data
print("Reading data...")
json_data = pd.read_json(DATA_DIR + FILE_NAME+JSON_EXT, lines=True)
data = json_data.to_dict('records')

print(f"Total rows : {len(data)}")

print("converting to csv ...")
new_data = []
#process data
for i in tqdm(range(len(data))):

    record = data[i]

    text = record['context'] 

    if len(text) > MAX_TEXT_LEN:

        text = text[:MAX_TEXT_LEN]

    question = record['qas'][0]['question']

    if len(question) > MAX_QUESTION_LEN:

        question = question[:MAX_QUESTION_LEN]

    answer_list = record['qas'][0]['answers_snippet_spans_bleu2']
    
    if len(answer_list) > 0:
        
        answers = []
        for ans in answer_list:
            
            answers.append(ans['text'])

        answers = ",".join(answers)
        print(answers)

        temp = {'text':text, 'question':question, 'answer':answers} 
        new_data.append(temp)

#convert to csv
df = pd.DataFrame(new_data)
df.to_csv(FILE_NAME + CSV_EXT, index=False)

