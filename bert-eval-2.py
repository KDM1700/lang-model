import torch
from transformers import BertForQuestionAnswering
from transformers import BertTokenizer
from tqdm import tqdm
import nltk
import json
import sys

#function to get answers given text and a question
def question_answer(question, text, model, tokenizer):
    
    #tokenize question and text in ids as a pair
    input_ids = tokenizer.encode(question, text)
    
    #string version of tokenized ids
    tokens = tokenizer.convert_ids_to_tokens(input_ids)

    if len(input_ids) > 500:

        return ""
    
    #segment IDs
    #first occurence of [SEP] token
    sep_idx = input_ids.index(tokenizer.sep_token_id)



    #number of tokens in segment A - question
    num_seg_a = sep_idx+1

    #number of tokens in segment B - text
    num_seg_b = len(input_ids) - num_seg_a
    
    #list of 0s and 1s
    segment_ids = [0]*num_seg_a + [1]*num_seg_b
    
    assert len(segment_ids) == len(input_ids)
    #model output using input_ids and segment_ids
    output = model(torch.tensor([input_ids]), token_type_ids=torch.tensor([segment_ids]))
    
    #reconstructing the answer
    answer_start = torch.argmax(output.start_logits)
    answer_end = torch.argmax(output.end_logits)

    if answer_end >= answer_start:
        answer = tokens[answer_start]
        for i in range(answer_start+1, answer_end+1):
            if tokens[i][0:2] == "##":
                answer += tokens[i][2:]
            else:
                answer += " " + tokens[i]
    
    else:

        return ""
                
    if answer.startswith("[CLS]") or answer.startswith("[SEP]") or answer == "[sep]":

        return ""
    
    
    answer =  answer.capitalize()
    return answer



DATA_DIR = "../data/"
FILE_NAME = "test-qar_squad_all.jsonl"

PATH = DATA_DIR+FILE_NAME

DATA = []

with open(PATH, 'r') as fp:

    for line in fp: 

        data_row = json.loads(line)  
        DATA.append(data_row)

OP = []

print(f"Total data samples - {len(DATA)}")
print(f"samples choesn for eval - {int(sys.argv[1])}")

DATA = DATA[:int(sys.argv[1])]

print("loading model ....")
model = BertForQuestionAnswering.from_pretrained('./bert-models/')
tokenizer = BertTokenizer.from_pretrained('./bert-models/')


for i in tqdm(range(len(DATA))):

    data_row = DATA[i]
    context = data_row["context"]

    context_sentences = nltk.sent_tokenize(context)
  
    
    question = data_row["qas"][0]["question"]
    

    ansList_bleu2 = data_row["qas"][0]["answers_snippet_spans_bleu2"] 
    ansList_bleu4 = data_row["qas"][0]["answers_snippet_spans_bleu4"] 
    human_answers = data_row["qas"][0]["human_answers"]


    answers_bleu2 = []
    answers_bleu4 = []

    for answerObj in ansList_bleu2:
        answers_bleu2.append(answerObj['text'])

    for answerObj in ansList_bleu4:
        answers_bleu4.append(answerObj['text'])
    
    
    generated_answers = []

    TEMP = {}

    ans = ""

    for context in context_sentences:
    
        ans = question_answer(question, context, model, tokenizer)
        #answer genrated
        if ans != "":
            break
    
    generated_answers.append(ans)
            

    TEMP["question"] = question
    TEMP["answers_bleu2"] = answers_bleu2
    TEMP["answers_bleu4"] = answers_bleu4
    TEMP["answers_human"] = human_answers 
    TEMP["generated_ans"] = generated_answers

    OP.append(TEMP)


for row in OP:

    json_object = json.dumps(row)
    with open(DATA_DIR + f"bert_eval_squad_2_{int(sys.argv[1])}.json", "a") as outfile:
        outfile.write(json_object)
        outfile.write('\n')

