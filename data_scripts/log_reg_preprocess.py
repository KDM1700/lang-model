#generate a small subset of data in jsonl format
from importlib import import_module
import json
import spacy
from tqdm import tqdm
import nltk
import numpy as np
from sentence_transformers import SentenceTransformer


DATA_DIR = "../../data/"
FILE_NAME = "test-qar_squad_all.jsonl"
PATH = DATA_DIR+FILE_NAME

SAVE_DIR = "../../drive/"
SAVE_FILE_NAME = "logreg_squad_25000_new.jsonl"
SAVE_PATH = SAVE_DIR + SAVE_FILE_NAME

NUM_FEAUTRES = 40


from nltk.stem.porter import *

stemmer = PorterStemmer()
def stemRoots(roots):
   
    stemmed_roots = []
    for root in roots:
        stemmed_roots.append(stemmer.stem(root))

    return stemmed_roots

nlp=spacy.load('en_core_web_sm')
def getRoots(sentence):
    roots = []

    for token in nlp(sentence):

        if token.dep_ == "ROOT":
            roots.append(token.text)

    return roots
            


def check_dependency(question_roots, context_roots):
    

    for qroot in question_roots:

        if qroot in context_roots:

            return 1

    return 0
    

def getDependencyScores(context_sentences,question):

    que_roots = getRoots(question)
    que_roots = stemRoots(que_roots) 

    results = []   

    for sen in context_sentences:

        sen_roots = getRoots(sen)
        sen_roots = stemRoots(sen_roots)

        result = check_dependency(que_roots, sen_roots)

        results.append(str(result))

    if len(results) < NUM_FEAUTRES:

        for i in range(len(results),NUM_FEAUTRES):

            results.append('0')

    return results


model = SentenceTransformer('bert-base-nli-mean-tokens')

def normalize_text(s):
    """Removing articles and punctuation, and standardizing whitespace are all typical text processing steps."""
    import string

    def white_space_fix(text):
        return " ".join(text.split())

    def remove_punc(text):
        exclude = set(string.punctuation)
        return "".join(ch for ch in text if ch not in exclude)

    def lower(text):
        return text.lower()

    return white_space_fix(remove_punc(lower(s)))

def cosine(u, v):
    return np.dot(u, v) / (np.linalg.norm(u) * np.linalg.norm(v))


def getCosineScores(context_sentences,question):

    sentences = []

    for sen in context_sentences:

        sentences.append(normalize_text(sen))

    que =  normalize_text(question)
    query_vec = model.encode([que])[0]

    results = []

    for sent in sentences:
        sim = cosine(query_vec, model.encode([sent])[0])
        results.append(str(sim))

    if len(results) < NUM_FEAUTRES:

        for i in range(len(results),NUM_FEAUTRES):

            results.append('0')

    return results

DATA = []

with open(PATH, 'r') as fp:

    for line in fp: 

        data_row = json.loads(line)  
        DATA.append(data_row)

print(f"Total data samples - {len(DATA)}")


OP = []

#25000 data samples

DATA = DATA[:25000]

for i in tqdm(range(len(DATA))):

    data_row = DATA[i]
    context = data_row["context"]
    question = data_row["qas"][0]["question"]

    context_sentences = nltk.sent_tokenize(context)
    
    answers_sentence_bleu2 = []
    answers_sentence_bleu4 = []
    answers_bleu2 = []
    answers_bleu4 = []
    


    ansList_sentence_bleu2 = data_row["qas"][0]["answers_sentence_bleu2"] 
    ansList_sentence_bleu4 = data_row["qas"][0]["answers_sentence_bleu4"] 
    ansList_bleu2 = data_row["qas"][0]["answers_snippet_spans_bleu2"] 
    ansList_bleu4 = data_row["qas"][0]["answers_snippet_spans_bleu4"] 
    human_answers = data_row["qas"][0]["human_answers"]


    for answerObj in ansList_bleu2:
        answers_bleu2.append(answerObj['text'])

    for answerObj in ansList_bleu4:
        answers_bleu4.append(answerObj['text'])

   
    for answerObj in ansList_sentence_bleu2:
        answers_sentence_bleu2.append(answerObj['text'])

    for answerObj in ansList_sentence_bleu4:
        answers_sentence_bleu4.append(answerObj['text'])

    ind  = context_sentences.index(answers_sentence_bleu2[0])
    
    #truncate to 40 sentences
    if ind < NUM_FEAUTRES:
        
    
        if len(context_sentences) > NUM_FEAUTRES:
            
            context_sentences = context_sentences[:NUM_FEAUTRES]

        target = str(ind)
        cosine_scores = getCosineScores(context_sentences, question)
        dependency_scores = getDependencyScores(context_sentences, question)
         
        TEMP = {}

        TEMP["context"] = context
        TEMP["question"] = question
        TEMP["target"] =  target
        TEMP["cosine_scores"] = cosine_scores
        TEMP["dependency_scores"] = dependency_scores
        TEMP["answers_sentence_bleu2"] = answers_sentence_bleu2
        TEMP["answers_sentence_bleu4"] = answers_sentence_bleu4
        TEMP["answers_bleu2"] = answers_bleu2
        TEMP["answers_bleu4"] = answers_bleu4
        TEMP["answers_human"] = human_answers 
        
    
        OP.append(TEMP)



for row in OP:

    json_object = json.dumps(row)
    with open(SAVE_PATH, "a") as outfile:
        outfile.write(json_object)
        outfile.write('\n')
