#generate a small subset of data in jsonl format
import json
import sys
from tqdm import tqdm

DATA_DIR = "../../data/"
FILE_NAME = "test-qar_squad_all.jsonl"
PATH = DATA_DIR+FILE_NAME



if len(sys.argv) > 1:

    NUM_SAMPLES = int(sys.argv[1])

#default
else:
    NUM_SAMPLES = 1000

SAVE_DIR = "../../drive/"
SAVE_FILE_NAME = f"test-qar_squad_{NUM_SAMPLES}.jsonl"
SAVE_PATH = SAVE_DIR + SAVE_FILE_NAME


DATA = []

with open(PATH, 'r') as fp:

    for line in fp: 

        data_row = json.loads(line)  
        DATA.append(data_row)

print(f"Total data samples - {len(DATA)}")
print(f"samples choesn for eval - {NUM_SAMPLES}")

DATA = DATA[:NUM_SAMPLES]

OP = []


for i in tqdm(range(len(DATA))):

    OP.append(DATA[i])


for row in OP:

    json_object = json.dumps(row)
    with open(SAVE_PATH, "a") as outfile:
        outfile.write(json_object)
        outfile.write('\n')