from numpy import mean
from numpy import std
import json
from tqdm import tqdm
from sklearn import metrics
from sklearn.linear_model import LogisticRegression
from sklearn.preprocessing import StandardScaler, MinMaxScaler
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier

DATA_DIR = "../../drive/"
FILE_NAME = "logreg_squad_5000.jsonl"
PATH = DATA_DIR+FILE_NAME


DATA = []

with open(PATH, 'r') as fp:

    for line in fp: 

        data_row = json.loads(line)  
        DATA.append(data_row)

print(f"Total data samples - {len(DATA)}")

X = []
Y = []


for i in tqdm(range(len(DATA))):

    data_row = DATA[i]
    x = data_row["scores"]
    y = data_row["target"]

    X.append(x)
    Y.append(y)


X = np.array(X).astype('float')
scaler = MinMaxScaler()
X = scaler.fit_transform(X)
Y = np.array(Y).astype('float')

train_x, test_x, train_y, test_y = train_test_split(X,
Y, train_size=0.8, random_state = 5)

mul_lr = LogisticRegression(multi_class='multinomial', solver='newton-cg')
mul_lr.fit(train_x, train_y)

print("Multinomial Logistic regression Train Accuracy : ", metrics.accuracy_score(train_y, mul_lr.predict(train_x)))
print("Multinomial Logistic regression Test Accuracy : ", metrics.accuracy_score(test_y, mul_lr.predict(test_x)))


rf = RandomForestClassifier(min_samples_leaf=8, n_estimators=60)
rf.fit(train_x, train_y)

print("Multinomial Logistic regression Train Accuracy : ", metrics.accuracy_score(train_y, rf.predict(train_x)))
print("Multinomial Logistic regression Test Accuracy : ", metrics.accuracy_score(test_y, rf.predict(test_x)))