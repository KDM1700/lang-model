#generate a small subset of data in jsonl format
import json
from tqdm import tqdm
import nltk
from sentence_transformers import SentenceTransformer
import numpy as np

DATA_DIR = "../../drive/"
FILE_NAME = "test-qar_squad_metrics.jsonl"
PATH = DATA_DIR+FILE_NAME


SAVE_DIR = "../../drive/"
SAVE_FILE_NAME = "result_cosine.jsonl"
SAVE_PATH = SAVE_DIR + SAVE_FILE_NAME

model = SentenceTransformer('bert-base-nli-mean-tokens')

def normalize_text(s):
    """Removing articles and punctuation, and standardizing whitespace are all typical text processing steps."""
    import string

    def white_space_fix(text):
        return " ".join(text.split())

    def remove_punc(text):
        exclude = set(string.punctuation)
        return "".join(ch for ch in text if ch not in exclude)

    def lower(text):
        return text.lower()

    return white_space_fix(remove_punc(lower(s)))

def cosine(u, v):
    return np.dot(u, v) / (np.linalg.norm(u) * np.linalg.norm(v))

def getAnswer(context, question):

    context_sentences = nltk.sent_tokenize(context)
    sentences = []

    for sen in context_sentences:

        sentences.append(normalize_text(sen))

    que =  normalize_text(question)
    query_vec = model.encode([que])[0]

    results = []

    for sent in sentences:
        sim = cosine(query_vec, model.encode([sent])[0])
        results.append((sim,sent))

    results.sort(reverse=True)
    return results[0][1]
    
DATA = []

with open(PATH, 'r') as fp:

    for line in fp: 

        data_row = json.loads(line)  
        DATA.append(data_row)

print(f"Total data samples - {len(DATA)}")


OP = []

for i in tqdm(range(len(DATA))):

    data_row = DATA[i]
    context = data_row["context"]
    id = data_row["qas"][0]["id"]
    question = data_row["qas"][0]["question"]

    ans = getAnswer(context,question)
    generated_answers = ans

    TEMP = {}

    TEMP["id"] = id
    TEMP["generated_ans"] = generated_answers

    OP.append(TEMP)


for row in OP:

    json_object = json.dumps(row)
    with open(SAVE_PATH, "a") as outfile:
        outfile.write(json_object)
        outfile.write('\n')
