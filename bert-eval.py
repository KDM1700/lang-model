import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import torch
from transformers import BertForQuestionAnswering
from transformers import BertTokenizer
from tqdm import tqdm
import sys

from scores import compute_f1
from scores import get_scores_bleu

#function to calulate bleu scores

if len(sys.argv) > 1:
    mode = sys.argv[1]
else:
    mode = "no_download"

DOWNLOAD_DATA = False
DOWNLOAD_MODEL = False

print('Running in Mode ' + mode)

if mode=="download":

    DOWNLOAD_DATA = True
    DOWNLOAD_MODEL = True


#function to get answers given text and a question
def question_answer(question, text, model, tokenizer):
    
    #tokenize question and text in ids as a pair
    input_ids = tokenizer.encode(question, text)
    
    #string version of tokenized ids
    tokens = tokenizer.convert_ids_to_tokens(input_ids)
    
    #segment IDs
    #first occurence of [SEP] token
    sep_idx = input_ids.index(tokenizer.sep_token_id)

    #number of tokens in segment A - question
    num_seg_a = sep_idx+1

    #number of tokens in segment B - text
    num_seg_b = len(input_ids) - num_seg_a
    
    #list of 0s and 1s
    segment_ids = [0]*num_seg_a + [1]*num_seg_b
    
    assert len(segment_ids) == len(input_ids)
    #model output using input_ids and segment_ids
    output = model(torch.tensor([input_ids]), token_type_ids=torch.tensor([segment_ids]))
    
    #reconstructing the answer
    answer_start = torch.argmax(output.start_logits)
    answer_end = torch.argmax(output.end_logits)

    if answer_end >= answer_start:
        answer = tokens[answer_start]
        for i in range(answer_start+1, answer_end+1):
            if tokens[i][0:2] == "##":
                answer += tokens[i][2:]
            else:
                answer += " " + tokens[i]
    
    else:

        return ""
                
    if answer.startswith("[CLS]") or answer == "[sep]":

        return ""
    
    
    answer =  answer.capitalize()
    return answer


print('Reading Data ..... ')
data = pd.read_csv("test-qar_squad_all.csv")
print("Number of question and answers: ", len(data))
data =  data.to_dict('records')[:1000]

if DOWNLOAD_MODEL:
    print("Downloading model....")
    model = BertForQuestionAnswering.from_pretrained('bert-large-uncased-whole-word-masking-finetuned-squad')
    tokenizer = BertTokenizer.from_pretrained('bert-large-uncased-whole-word-masking-finetuned-squad')

    tokenizer.save_pretrained('./bert-models/')
    model.save_pretrained('./bert-models/')


print("loading model ....")
model = BertForQuestionAnswering.from_pretrained('./bert-models/')
tokenizer = BertTokenizer.from_pretrained('./bert-models/')


gold_answers_dict = []
op_answers_dict = []

print("generating machine answers ...")

with open("bert-op.txt", "w") as file:

    for i in tqdm(range(len(data))):

        record = data[i]

        txt = record['text']
        que = record['question']
        gold_answers = record['answer']
        ans = question_answer(que, txt, model, tokenizer)

        if len(ans) != 0 and ans != "[sep]":

            file.write(f"que - {que} \n\n")
            file.write(f"gold answers - {gold_answers} \n\n")
            file.write(f"generated ans - {ans} \n\n")

            gold_answers_dict.append(gold_answers)
            op_answers_dict.append(ans)


print(f"Total answerable questions - {len(gold_answers_dict)}")

print("Calculating scores ....")
get_scores_bleu(gold_answers_dict, op_answers_dict)
print(compute_f1(gold_answers_dict, op_answers_dict))