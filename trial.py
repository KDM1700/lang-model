import pandas as pd
import json


DATA_DIR = "../data/"
FILE_NAME = "test-qar_all.jsonl"

PATH = DATA_DIR+FILE_NAME

IS_ANSWERABLE = 'is_answerable'
REVIEW_SNIPPETS = 'review_snippets'
QUESTIONS = 'questions'
QUESTION_TEXT = 'questionText'
ANSWER_TEXT = 'answerText'
ANSWERS = 'answers'
NUM_REVIEWS = 5

DATA = []

with open(PATH, 'r') as fp:

    for line in fp:  
        data_row = {}      
        row = json.loads(line)  
        is_answerable = row[IS_ANSWERABLE] 
        question = row[QUESTION_TEXT]
        answers = row[ANSWERS]
        reviews = row[REVIEW_SNIPPETS][:NUM_REVIEWS]   
        ansList = []

        for ans in answers:

            ansText = ans[ANSWER_TEXT]
            ansList.append(ansText)


        data_row["question"] = question
        data_row["is_answerable"] = is_answerable
        data_row["reviews"] = reviews
        data_row["answers"] = ansList

        DATA.append(data_row)

for row in DATA:

    json_object = json.dumps(row)
    with open(DATA_DIR + "new_ds.json", "a") as outfile:
        outfile.write(json_object)
        outfile.write('\n')

