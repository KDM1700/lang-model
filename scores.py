# these functions are heavily influenced by the HF squad_metrics.py script
from nltk.translate.bleu_score import SmoothingFunction
import nltk
from tqdm import tqdm

def normalize_text(s):
    """Removing articles and punctuation, and standardizing whitespace are all typical text processing steps."""
    import string, re

    def remove_articles(text):
        regex = re.compile(r"\b(a|an|the)\b", re.UNICODE)
        return re.sub(regex, " ", text)

    def white_space_fix(text):
        return " ".join(text.split())

    def remove_punc(text):
        exclude = set(string.punctuation)
        return "".join(ch for ch in text if ch not in exclude)

    def lower(text):
        return text.lower()

    return white_space_fix(remove_articles(remove_punc(lower(s))))

def compute_exact_match(prediction, truth):
    return int(normalize_text(prediction) == normalize_text(truth))


def cal_score_f1(gold_ans, gen_ans):

    gold_ans = normalize_text(gold_ans)
    gen_ans = normalize_text(gen_ans)

    pred_tokens = gen_ans.split()
    truth_tokens = gold_ans.split()
    # if either the prediction or the truth is no-answer then f1 = 1 if they agree, 0 otherwise
    if len(pred_tokens) == 0 or len(truth_tokens) == 0:
    
        return int(pred_tokens == truth_tokens)
    
    common_tokens = set(pred_tokens) & set(truth_tokens)

    # if there are no common tokens then f1 = 0
    if len(common_tokens) == 0:
            
        return 0

    prec = len(common_tokens) / len(pred_tokens)
    rec = len(common_tokens) / len(truth_tokens)
    return 2 * (prec * rec) / (prec + rec)


def compute_f1(gold_answers, generated_answers):

    num_samples = len(gold_answers)
    avg_score = 0
    

    for i in range(num_samples):

        prediction = generated_answers[i]
        truth_answers = gold_answers[i].split(",")
        score = 0

        for truth in truth_answers:

            curr_score = cal_score_f1(truth, prediction)
            score = max(score,curr_score)

        avg_score += score

    return avg_score/num_samples

def get_scores_bleu(gold_dict, answer_dict):

    bleu_1_total = 0
    bleu_2_total = 0
    bleu_3_total = 0
    bleu_4_total = 0

    for i in tqdm(range(len(gold_dict))):

        hp = answer_dict[i]
        re = gold_dict[i]

        hypothesis = hp.split()
        reference = re.split()
        #there may be several references
        chencherry = SmoothingFunction()
        BLEU_1 = nltk.translate.bleu_score.sentence_bleu([reference], hypothesis, weights=(1,0,0,0), smoothing_function=chencherry.method1)
        BLEU_2 = nltk.translate.bleu_score.sentence_bleu([reference], hypothesis, weights=(1.0/2.0,1.0/2.0),smoothing_function=chencherry.method1)
        BLEU_3 = nltk.translate.bleu_score.sentence_bleu([reference], hypothesis, weights=(1.0/3.0,1.0/3.0,1.0/3.0), smoothing_function=chencherry.method1)
        BLEU_4 = nltk.translate.bleu_score.sentence_bleu([reference], hypothesis, smoothing_function=chencherry.method1)
        
        bleu_1_total += BLEU_1
        bleu_2_total += BLEU_2
        bleu_3_total += BLEU_3
        bleu_4_total += BLEU_4
    
    print(f"BLEU 1 : {bleu_1_total/len(gold_dict)}")
    print(f"BLEU 2 : {bleu_2_total/len(gold_dict)}")
    print(f"BLEU 3 : {bleu_3_total/len(gold_dict)}")
    print(f"BLEU 4 : {bleu_4_total/len(gold_dict)}")


