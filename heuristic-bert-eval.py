#test bert model on various heuristics from the paper
##MODE reference
##sys arg 1

'''
1 - random sentence
2 - top_review_helpful
3 - top_review_wilson
4 - top_sentences_IR
'''

import json
from multiprocessing import context
import sys
import torch
from transformers import BertForQuestionAnswering
from transformers import BertTokenizer
from tqdm import tqdm

MODES = ["random_sentence","top_review_helpful","top_review_wilson","top_sentences_IR"]

MODE = int(sys.argv[1])
NUM_SAMPLES = int(sys.argv[2])
MAX_BERT_TOKENS = 512

DATA_DIR = "../data/"
FILE_NAME = "test-qar_all.jsonl"
PATH = DATA_DIR+FILE_NAME

SAVE_DIR = "../drive/"
SAVE_FILE_NAME = f"heuristic_bert_eval_{MODES[MODE-1]}_{NUM_SAMPLES}.json"
SAVE_PATH = SAVE_DIR + SAVE_FILE_NAME


#function to get answers given text and a question
def question_answer(question, text, model, tokenizer):
    
    #tokenize question and text in ids as a pair
    input_ids = tokenizer.encode(question, text)

    if len(input_ids) > 512:
        input_ids = input_ids[:512]

    #string version of tokenized ids
    tokens = tokenizer.convert_ids_to_tokens(input_ids)    
    #segment IDs
    #first occurence of [SEP] token
    sep_idx = input_ids.index(tokenizer.sep_token_id)
    #number of tokens in segment A - question
    num_seg_a = sep_idx+1
    #number of tokens in segment B - text
    num_seg_b = len(input_ids) - num_seg_a
    
    #list of 0s and 1s
    segment_ids = [0]*num_seg_a + [1]*num_seg_b
    
    assert len(segment_ids) == len(input_ids)
    #model output using input_ids and segment_ids
    output = model(torch.tensor([input_ids]), token_type_ids=torch.tensor([segment_ids]))
    
    #reconstructing the answer
    answer_start = torch.argmax(output.start_logits)
    answer_end = torch.argmax(output.end_logits)

    if answer_end >= answer_start:
        answer = tokens[answer_start]
        for i in range(answer_start+1, answer_end+1):
            if tokens[i][0:2] == "##":
                answer += tokens[i][2:]
            else:
                answer += " " + tokens[i]
    
    else:

        return ""
                
    if answer.startswith("[CLS]") or answer.startswith("[SEP]") or answer == "[SEP]":

        return ""
    
    
    answer =  answer.capitalize()
    return answer


DATA = []

with open(PATH, 'r') as fp:

    for line in fp: 

        data_row = json.loads(line)  
        DATA.append(data_row)


print(f"Filename - {FILE_NAME}")
print(f"Total data samples - {len(DATA)}")
print(f"samples choesn for eval - {NUM_SAMPLES}")
print(f"Mode - {MODES[MODE-1]}")

DATA = DATA[:NUM_SAMPLES]
OP = []


print("loading model ....")
model = BertForQuestionAnswering.from_pretrained('./bert-models/')
tokenizer = BertTokenizer.from_pretrained('./bert-models/')

for i in tqdm(range(len(DATA))):

    data_row = DATA[i]
    question = data_row["questionText"]
    reviews = data_row[MODES[MODE-1]]
    context = ' '.join(reviews)
    answerList = data_row["answers"]

    generated_answers = []

    ans = question_answer(question, context, model, tokenizer)
    generated_answers.append(ans)

    human_answers = []

    for answer in answerList:
        human_answers.append(answer["answerText"])


    TEMP = {}
    
    TEMP["context"] = context
    TEMP["question"] = question
    TEMP["answers_human"] = human_answers
    TEMP["generated_answers"] = generated_answers


    OP.append(TEMP)

for row in OP:

    json_object = json.dumps(row)
    with open(SAVE_PATH , "a") as outfile:
        outfile.write(json_object)
        outfile.write('\n')

